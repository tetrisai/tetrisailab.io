    var gridCanvas = document.getElementById('grid-canvas');
    var nextCanvas = document.getElementById('next-canvas');
    var scoreContainer = document.getElementById("score-container");
    var resetButton = document.getElementById('reset-button');
    var aiButton = document.getElementById('ai-button');
    var gridContext = gridCanvas.getContext('2d');
    var nextContext = nextCanvas.getContext('2d');
    document.addEventListener('keydown', onKeyDown);

    var grid = new Grid(22, 10);
    //pieces=[];
    commands=[];
    shapes=[[[400, 401, 402, 403], [302, 402, 502, 602], [400, 401, 402, 403], [302, 402, 502, 602]], [[400, 401, 402, 502], [402, 403, 502, 602], [302, 402, 403, 404], [202, 302, 401, 402]], [[302, 400, 401, 402], [401, 402, 502, 602], [402, 403, 404, 502], [202, 302, 402, 403]], [[302, 402, 403, 502], [302, 401, 402, 403], [302, 401, 402, 502], [401, 402, 403, 502]], [[401, 402, 501, 502], [401, 402, 501, 502], [401, 402, 501, 502], [401, 402, 501, 502]], [[302, 401, 402, 501], [301, 302, 402, 403], [302, 401, 402, 501], [301, 302, 402, 403]], [[301, 401, 402, 502], [302, 303, 401, 402], [301, 401, 402, 502], [302, 303, 401, 402]]]
    //grid_global=grid;
    var rpg = new RandomPieceGenerator();
    var ai = new AI({
        heightWeight: 0.510066,
        linesWeight: 0.760666,
        holesWeight: 0.35663,
        bumpinessWeight: 0.184483
    });
    var workingPieces = [null, rpg.nextPiece()];
    var workingPiece = null;
    var isAiActive = true;
    var isKeyEnabled = false;
    var gravityTimer = new Timer(onGravityTimerTick, 500);
    var score = 0;
    var killscore=999999999;
    //var milestone=0;
    var nodraw=0;
    function drawall(){
        nodraw=0;
        redrawGridCanvas();
        redrawNextCanvas();
        updateScoreContainer();
    }

    // Graphics
    function intToRGBHexString(v){
        return 'rgb(' + ((v >> 16) & 0xFF) + ',' + ((v >> 8) & 0xFF) + ',' + (v & 0xFF) + ')';
    }

    function redrawGridCanvas(workingPieceVerticalOffset = 0){
        if(nodraw) return;
        gridContext.save();

        // Clear
        gridContext.clearRect(0, 0, gridCanvas.width, gridCanvas.height);

        // Draw grid
        for(var r = 2; r < grid.rows; r++){
            for(var c = 0; c < grid.columns; c++){
                if (grid.cells[r][c] != 0){
                    gridContext.fillStyle= intToRGBHexString(grid.cells[r][c]);
                    gridContext.fillRect(20 * c, 20 * (r - 2), 20, 20);
                    gridContext.strokeStyle="#FFFFFF";
                    gridContext.strokeRect(20 * c, 20 * (r - 2), 20, 20);
                }
            }
        }

        // Draw working piece
        for(var r = 0; r < workingPiece.dimension; r++){
            for(var c = 0; c < workingPiece.dimension; c++){
                if (workingPiece.cells[r][c] != 0){
                    gridContext.fillStyle = intToRGBHexString(workingPiece.cells[r][c]);
                    gridContext.fillRect(20 * (c + workingPiece.column), 20 * ((r + workingPiece.row) - 2) + workingPieceVerticalOffset, 20, 20);
                    gridContext.strokeStyle="#FFFFFF";
                    gridContext.strokeRect(20 * (c + workingPiece.column), 20 * ((r + workingPiece.row) - 2) + workingPieceVerticalOffset, 20, 20);
                }
            }
        }

        gridContext.restore();
    }

    function redrawNextCanvas(){
        if(nodraw) return;
        nextContext.save();

        nextContext.clearRect(0, 0, nextCanvas.width, nextCanvas.height);
        var next = workingPieces[1];
        var xOffset = next.dimension == 2 ? 20 : next.dimension == 3 ? 10 : next.dimension == 4 ? 0 : null;
        var yOffset = next.dimension == 2 ? 20 : next.dimension == 3 ? 20 : next.dimension == 4 ? 10 : null;
        for(var r = 0; r < next.dimension; r++){
            for(var c = 0; c < next.dimension; c++){
                if (next.cells[r][c] != 0){
                    nextContext.fillStyle = intToRGBHexString(next.cells[r][c]);
                    nextContext.fillRect(xOffset + 20 * c, yOffset + 20 * r, 20, 20);
                    nextContext.strokeStyle = "#FFFFFF";
                    nextContext.strokeRect(xOffset + 20 * c, yOffset + 20 * r, 20, 20);
                }
            }
        }

        nextContext.restore();
    }

    function updateScoreContainer(){
        //if(nodraw) return;
        scoreContainer.innerHTML = score.toString();
    }

    // Drop animation
    var workingPieceDropAnimationStopwatch = null;

    function startWorkingPieceDropAnimation(callback = function(){}){
        // Calculate animation height
        animationHeight = 0;
        _workingPiece = workingPiece.clone();
        while(_workingPiece.moveDown(grid)){
            animationHeight++;
        }

        var stopwatch = new Stopwatch(function(elapsed){
            if(elapsed >= animationHeight * 20){
                stopwatch.stop();
                //redrawGridCanvas(20 * animationHeight);
                callback();
                return;
            }

            //redrawGridCanvas(20 * elapsed / 20);
        });

        workingPieceDropAnimationStopwatch = stopwatch;
    }

    function cancelWorkingPieceDropAnimation(){
        if(workingPieceDropAnimationStopwatch === null){
            return;
        }
        workingPieceDropAnimationStopwatch.stop();
        workingPieceDropAnimationStopwatch = null;
    }

    // Process start of turn
    function startTurn(){
        // Shift working pieces
        for(var i = 0; i < workingPieces.length - 1; i++){
            workingPieces[i] = workingPieces[i + 1];
        }
        workingPieces[workingPieces.length - 1] = rpg.nextPiece();
        workingPiece = workingPieces[0];

        // Refresh Graphics
        redrawGridCanvas();
        redrawNextCanvas();

        if(isAiActive){
            isKeyEnabled = false;
            workingPiece = ai.best(grid, workingPieces);
            startWorkingPieceDropAnimation(function(){
                while(workingPiece.moveDown(grid)); // Drop working piece
                if(!endTurn()){
                    alert('Game Over!');
                    loadStatusfromlist(statuses.length-10);
                    return;
                }
                startTurn();
            })
        }else{
            isKeyEnabled = true;
            gravityTimer.resetForward(500);
        }
    }
    function getCommand(piece,wp){
        //console.log(piece);
        for(var i=0;i<=3;i++) {
            //console.log(wp)
            var shape=shapes[wp.index][(i+rpg.index-3)%4];
            //console.log(shape);
            if(shape[0]-piece[0]==shape[1]-piece[1]&&shape[0]-piece[0]==shape[2]-piece[2]&&shape[0]-piece[0]==shape[3]-piece[3]){
                var cmd="N,"
                if(i!=0) cmd+="C"+i+","
                var s1=Math.round((piece[0]-shape[0])/100)
                if(s1<0) cmd+="L"+(-s1)+","
                if(s1>0) cmd+="R"+s1+","
                var s2=piece[0]-shape[0]-s1*100
                if(s2>0) cmd+="D"+s2+","
                return cmd
            }
        }
        alert("Error!")
    }
    // Process end of turn
    storeCommands=function(){document.getElementsByClassName("footer")[0].innerHTML="<textarea id='cmd'>"+commands.slice(0,10000).join("").slice(0,-1)+"</textarea>";}
    function endTurn(){
        // Add working piece
        var curPiece=grid.addPiece(workingPiece);
        //pieces.push(curPiece);
        commands.push(getCommand(curPiece,workingPiece));
        
        var items=grid.cells.flat().map(x=>(x!=0)).reduce((x,y)=>x+y, 0)
        var lines=grid.clearLines()
        // Clear lines
        score += items*lines*(lines+1)/2;
        if(score>killscore){
            killscore=99999999;
            isAiActive=false;
            drawall();
        }

        // Refresh graphics
        redrawGridCanvas();
        updateScoreContainer();
        if(lines>0) storeStatus();
        //if(score-milestone>10000){
        //    storeStatus();
        //    milestone+=10000;
        //}
        
        if(grid.exceeded()){
            storeCommands();
            return 0;
        } else {
            return 1;
        }
    }

    // Process gravity tick
    function onGravityTimerTick(){
        // If working piece has not reached bottom
        if(workingPiece.canMoveDown(grid)){
            workingPiece.moveDown(grid);
            redrawGridCanvas();
            return;
        }

        // Stop gravity if working piece has reached bottom
        gravityTimer.stop();

        // If working piece has reached bottom, end of turn has been processed
        // and game cannot continue because grid has been exceeded
        if(!endTurn()){
            isKeyEnabled = false;
            alert('Game Over!');
            return;
        }

        // If working piece has reached bottom, end of turn has been processed
        // and game can still continue.
        startTurn();
    }

    // Process keys
    function onKeyDown(event){
        if(!isKeyEnabled){
            return;
        }
        switch(event.which){
            case 32: // spacebar
                isKeyEnabled = false;
                gravityTimer.stop(); // Stop gravity
                startWorkingPieceDropAnimation(function(){ // Start drop animation
                    while(workingPiece.moveDown(grid)); // Drop working piece
                    if(!endTurn()){
                        alert('Game Over!');
                        return;
                    }
                    startTurn();
                });
                event.preventDefault();
                break;
            case 40: // down
                gravityTimer.resetForward(500);
                event.preventDefault();
                break;
            case 37: //left
                if(workingPiece.canMoveLeft(grid)){
                    workingPiece.moveLeft(grid);
                    redrawGridCanvas();
                }
                event.preventDefault();
                break;
            case 39: //right
                if(workingPiece.canMoveRight(grid)){
                    workingPiece.moveRight(grid);
                    redrawGridCanvas();
                }
                event.preventDefault();
                break;
            case 38: //up
                workingPiece.rotate(grid);
                redrawGridCanvas();
                event.preventDefault();
                break;
            case 188:
                loadStatusfromlist(Math.max(curstatusid-1,0));
                event.preventDefault();
                break;
            case 190:
                loadStatusfromlist(Math.min(curstatusid+1,statuses.length-1));
                event.preventDefault();
                break;
            case 191:
                storeCommands();
                event.preventDefault();
                break;
            case 90:
                storeStatus();
                event.preventDefault();
                break;
        }
    }

    aiButton.onclick = function(){
        if (isAiActive){
            isAiActive = false;
            aiButton.style.backgroundColor = "#f9f9f9";
        }else{
            isAiActive = true;
            aiButton.style.backgroundColor = "#e9e9ff";

            isKeyEnabled = false;
            gravityTimer.stop();
            startWorkingPieceDropAnimation(function(){ // Start drop animation
                while(workingPiece.moveDown(grid)); // Drop working piece
                if(!endTurn()){
                    alert('Game Over!');
                    return;
                }
                startTurn();
            });
        }
    }

    resetButton.onclick = function(){
        //pieces=[];
        commands=[];
        gravityTimer.stop();
        cancelWorkingPieceDropAnimation();
        grid = new Grid(22, 10);
        rpg = new RandomPieceGenerator();
        workingPieces = [null, rpg.nextPiece()];
        workingPieces[2]=rpg.nextPiece();
        /*workingPieces[3]=rpg.nextPiece();
        workingPieces[4]=rpg.nextPiece();
        workingPieces[5]=rpg.nextPiece();
        workingPieces[6]=rpg.nextPiece();
        workingPieces[7]=rpg.nextPiece();
        workingPieces[8]=rpg.nextPiece();
        workingPieces[9]=rpg.nextPiece();
        workingPieces[10]=rpg.nextPiece();*/
        workingPiece = null;
        score = 0;
        isKeyEnabled = true;
        updateScoreContainer();
        startTurn();
    }
    var statuses=[]
    var scores={}
    storeStatus=function(){
        statuses.push([[...commands],grid.clone(),rpg.clone(),workingPieces.map(x=>(x?x.clone():null)),score]);
        if(!scores[score]) scores[score]=statuses.length-1;
        curstatusid=statuses.length-1;
    }
    loadStatus=function(s){
        commands=[...s[0]];
        grid=s[1].clone();
        rpg=s[2].clone();
        workingPieces=s[3].map(x=>(x?x.clone():null))
        score=s[4]
        //milestone=score-score%10000
        drawall()
    }
    var curstatusid=0;
    loadStatusfromlist=function(id){
        curstatusid=id;
        loadStatus(statuses[id]);isAiActive=false;startTurn();
    }

    aiButton.style.backgroundColor = "#e9e9ff";
