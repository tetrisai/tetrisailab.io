function Stopwatch(callback) {
    var isStopped = false;
    var startDate = null;
    var self = this;

    var onAnimationFrame = function(){
        if(isStopped){
            return;
        }
        callback(99999);
        requestAnimationFrame(onAnimationFrame);
    };

    this.stop = function() {
        isStopped = true;
    }

    startDate = Date.now();
    requestAnimationFrame(onAnimationFrame);
}
