/**
 * @param {Object} weights
 * @param {number} weights.heightWeight
 * @param {number} weights.linesWeight
 * @param {number} weights.holesWeight
 * @param {number} weights.bumpinessWeight
 */
function AI(weights){
    this.heightWeight = weights.heightWeight;
    this.linesWeight = weights.linesWeight;
    this.holesWeight = weights.holesWeight;
    this.bumpinessWeight = weights.bumpinessWeight;
};

AI.prototype._best = function(grid, workingPieces, workingPieceIndex, lines=0, old_p1=0){
    var best = null;
    var bestScore = null;
    var workingPiece = workingPieces[workingPieceIndex];

    for(var rotation = 0; rotation < 4; rotation++){
        var _piece = workingPiece.clone();
        for(var i = 0; i < rotation; i++){
            _piece.rotate(grid);
        }

        while(_piece.moveLeft(grid));

        while(grid.valid(_piece)){
            var _pieceSet = _piece.clone();
            while(_pieceSet.moveDown(grid));

            var _grid = grid.clone();
            _grid.addPiece(_pieceSet);

            var score = null;
            var p1=_grid.lines();
            var p2=_grid.maximumHeight();
            var new_lines=lines+(p1-old_p1)*(p1-old_p1+1)/2*(grid.cells.flat().map(x=>(x!=0)).reduce((x,y)=>x+y, 0)/100);
            if(p2>=20&&p1==0) score=-999999999;
            else if (workingPieceIndex == (workingPieces.length - 1)||(workingPieceIndex == 1&&p1<=15)) {
                score = -this.heightWeight * _grid.aggregateHeight() + this.linesWeight * new_lines * 6 - this.holesWeight * _grid.holes() - this.bumpinessWeight * _grid.bumpiness();
            }else{
                score = this._best(_grid, workingPieces, workingPieceIndex + 1, new_lines, p1).score;
            }

            if (score > bestScore || bestScore == null){
                bestScore = score;
                best = _piece.clone();
            }

            _piece.column++;
        }
    }

    return {piece:best, score:bestScore};
};

AI.prototype.best = function(grid, workingPieces){
    return this._best(grid, workingPieces, 0).piece;
};
