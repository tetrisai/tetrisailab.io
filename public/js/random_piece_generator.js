function RandomPieceGenerator(){
    this.index=0;
    this.rand_v=12358;
};

RandomPieceGenerator.prototype.nextPiece = function(){
    this.index++;
    if(this.index==10003) alert("end");
    //console.log(this.rand_v%29);
    this.rand_v=(this.rand_v*27073+17713)%32749;
    //console.log(this.rand_v%29);
    if(this.rand_v%29<=1) return Piece.fromIndex(6,0);
    else if(this.rand_v%29<=4) return Piece.fromIndex(2,1);
    else if(this.rand_v%29<=7) return Piece.fromIndex(1,2);
    else if(this.rand_v%29<=11) return Piece.fromIndex(5,3);
    else if(this.rand_v%29<=16) return Piece.fromIndex(0,4);
    else if(this.rand_v%29<=22) return Piece.fromIndex(4,5);
    else return Piece.fromIndex(3,6);
};
RandomPieceGenerator.prototype.clone = function(){
    _tmp=new RandomPieceGenerator();
    _tmp.index=this.index;
    _tmp.rand_v=this.rand_v;
    return _tmp;
}