# Tetris AI
This is a fork of [Tetris AI by Yiyuan Lee](https://github.com/LeeYiyuan/tetrisai). See [here](https://cloud.tencent.com/developer/competition/introduction/10015) for a documentation of the rule it uses.

See [here](https://codemyroad.wordpress.com/2013/04/14/tetris-ai-the-near-perfect-player/) for an introduction of the AI.

## License
Copyright (C) 2014 - 2021 [Yiyuan Lee](https://leeyiyuan.info) and Jack Zhang
[MIT License](https://github.com/LeeYiyuan/tetrisai/blob/gh-pages/License.md)
